attributes: [OrderedDict with dataframes as values(whereever dictionary used to be) oriented with rows] use '' for accessing value corresp to key

storages: [list]
['Battery', 'Hot Water Tank']

capacity_storage: [dataframe]
                capacity_storage
Battery                   0.0000
Hot Water Tank           21.0085

energy_to_storage: [dataframe]
    Battery  Hot Water Tank
0       0.0         0.00000
1       0.0         0.00000
2       0.0         0.00000
3       0.0         0.00000
4       0.0         0.00000
5       0.0         0.00000
6       0.0         0.00000
7       0.0         5.31315
8       0.0         5.31315
9       0.0         5.31315
10      0.0         5.31315

energy_from_storage: [dataframe]
    Battery  Hot Water Tank
0       0.0         4.60000
1       0.0         2.68685
2       0.0         2.68685
3       0.0         2.68685
4       0.0         2.68685
5       0.0         2.68685
6       0.0         2.68685
7       0.0         0.00000
8       0.0         0.00000
9       0.0         0.00000
10      0.0         0.00000

storage_level: [dataframe]
    Battery  Hot Water Tank
0       0.0        21.00850
1       0.0        16.34110
2       0.0        13.61070
3       0.0        10.88310
4       0.0         8.15827
5       0.0         5.43613
6       0.0         2.71670
7       0.0         0.00000
8       0.0         5.26002
9       0.0        10.51480
10      0.0        15.76430
11      0.0        21.00850