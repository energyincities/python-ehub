"""
Provides functionality for creating and solving an energy hub model.
"""
from energy_hub.ehub_model import EHubModel
