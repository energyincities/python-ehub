from pyehub.energy_hub.ehub_model import EHubModel
from pyehub.energy_hub.utils      import constraint
from pyehub.pylp                  import RealVariable, BinaryVariable
from pyehub.pylp                  import IntegerVariable
from pyehub.outputter             import output_excel,pretty_print


from datetime                     import datetime, timedelta
from time                         import time
from calendar                     import monthrange

import calendar
import numpy             as np
import pandas            as pd
import matplotlib.pyplot as plt

import logging
logger = logging.getLogger()
logger.setLevel(logging.ERROR)

class pvRoofModel(EHubModel):
    '''
    This is a version of the ehub that has a 
    couple of features that are used both in 
    the Ontario and the BC model.
    
    This model has the Roof model built in.
    '''
    def __init__(self, *, excel=None, 
                 request=None,
                 MAX_ROOF_AREA=None,
                 ROOF_AREA_CAP=None,
                 big_m=None):
        # This innitiates the EHUB model
        super().__init__(excel=excel, request=request,big_m=big_m)
        self.MAX_ROOF_AREA  = MAX_ROOF_AREA
        self.roof_area_per_unit_capacity = ROOF_AREA_CAP
        
    def _add_variables(self):
        """
        This function add the variables that the ehub will optmize
        to find the least cost solution. 
        """
        # This makes sure to run also the EHUB variable function
        super()._add_variables()
        # This is the total roof area from the building
        # we have this real variable here such that it 
        # shows up on the results
        self.total_roof_area      = RealVariable()
        # This is the total roof area used by the solar
        # panels. 
        self.total_roof_area_used = RealVariable()

    @constraint()
    def roof_tech_area_definition(self):
        """ 
        This calculates the roof area and adds it to the output file
        """

        # multiply area-per-unit-capacity and capacity for all roof-techs and sum all of them
        total_roof_area_used = sum((self.capacities[tech_name])*(self.roof_area_per_unit_capacity[tech_name])
                              for tech_name in self.roof_area_per_unit_capacity)
                
        return self.total_roof_area_used == total_roof_area_used      
    
    @constraint()
    def total_roof_area_definition(self):
        """ 
        This adds the total roof area to the output file 
        """
        return self.total_roof_area == float(self.MAX_ROOF_AREA)
    
    @constraint()
    def roof_tech_area_below_max(self):
        """ 
        Ensure the roof techs are taking up less area than there is roof.
        """

        # multiply area-per-unit-capacity and capacity for all roof-techs and sum all of them
        total_roof_area_used = sum((self.capacities[tech_name])*(self.roof_area_per_unit_capacity[tech_name])
                              for tech_name in self.roof_area_per_unit_capacity)
                
        return total_roof_area_used <= float(self.MAX_ROOF_AREA)
    
    def set_irr(self, i, irr):
        """
        Set the irradiance for the pv model
        param: i - panel number
        param: irr - an array with the irradience in kW per m2
        """
        self.TIME_SERIES['Irra{}'.format(i)] = {t: value for t, value in enumerate(irr)}
        
    def set_lin_cap_cost(self, i, cost):
        """
        Set the linear capital cost for the pv system
        param: i - panel number
        param: cost - the linear cost of the panel in $/m2
        """
        self.LINEAR_CAPITAL_COSTS['PV{}'.format(i)] = cost
    
    def set_load(self,Load):
        """
        Set the electricity load for each timestep 
        param: Load - An array that has the hourly load of the building in kWh
        """
        new_elec = {t: value for t, value in enumerate(Load)}
        self.LOADS['Elec'] = new_elec 
    
    def make_pie_chart(self):
        """
        Cool analysis function, this takes the costBreak variable made after
        evaluating the model using eval_model and makes a nice piechart. 
        """
        if (self.energyIm == 0):
            print("No energy is imported from the grid")
        else:
            print("Total Electricity Imported from the grid {:,.0f} kWh".format(self.energyIm))
            print("At a cost of $ {:,.4f} /kWh".format(self.costBreak["Energy Charge"]/self.energyIm))
        print("Total Cost of the system: $ {:,.0f} per year".format(self.totCost))
        
        cleaned_up_dict = self.costBreak
        
        cleaned_up_dict={k:v for k,v in cleaned_up_dict.items() if round(v) != 0}
        
        fig1, ax1 = plt.subplots()
        fig1.set_facecolor('lightgrey')
        

        ax1.pie(cleaned_up_dict.values(),\
                labels=cleaned_up_dict.keys(), autopct='%1.1f%%', \
                shadow=False, startangle=90);
        ax1.axis('equal')  
        plt.show();
        
    def roof_percentage(self):
        """
        This finds the percentage of the roof coverage. 
        """
        
        total_roof_area      = self.solveResults['solution']['total_roof_area']
        total_roof_area_used = self.solveResults['solution']['total_roof_area_used']
        percentage       = (total_roof_area_used/total_roof_area)*100
        print("Total roof area of the  building: {:,.0f} m²".format(total_roof_area))
        print("Total roof area used {:,.0f} m²".format(total_roof_area_used))
        print("Percentage: {:3.1f}%".format(percentage)) 
        
    def generated_percentage(self):
        """
        This finds the total electricity needed by the building and how much the pv
        panels contribute to this total amount. 
        """
        load_electricity = self.energyLoad
        load_pro         = self.energyProd
        percentage       = (load_pro/load_electricity)*100
        print("Total Electricity Load from the building: {:,.0f} kWh".format(load_electricity))
        print("Total Electricity Generated from PV: {:,.0f} kWh".format(load_pro))
        print("Percentage: {:3.1f}%".format(percentage))      
    
class ModelBC(pvRoofModel):
    """
    This is a subclass of pvRoofModel that adds BC model specific grid charges.
    """
    
    def __init__(self, *, excel=None, 
                 request=None,
                 max_carbon=0,
                 pvbuilding=None,
                 ROOF_AREA_CAP=0,
                 resultsFile=None,
                 NUM_PANELS=3,
                 factor_multiplier=1.5): # 1.5 is based on trial and error.
        self.factor_multiplier = factor_multiplier
        self.roof_area_per_unit_capacity = ROOF_AREA_CAP
        MAX_ROOF_AREA = pvbuilding.roof_area
        self.service_rate = pvbuilding.check_bc_rate()



        # We found that the model does wierd things
        # when varying BIG_M the following functions 
        # Set BIG M to various settings to prevent issues
        # The default should work in 99% of the time. 
        # when we have an error that M is too small we go 
        # bigger using the multiplier, if it is too big 
        # we recalibrate. This is all trail and error. 
        # we need to solve these issues in the long term
        # when the paper is complete. 
        dependent_var = pvbuilding.max_demand()
        if (dependent_var > 99999):
            # Should be much bigger than 550.000, and also significantly bigger than the 
            # total demand used by the building. because: large-550 > 1 * Massive
            # if Massive smaller than the problem becomes an issue
            
            # We use big_m in our code to make a couple binaries work
            # They pivot around this depedent var. 
            # 1.5 is based on trial and error.
            if (self.factor_multiplier==1.1):
                self.BIG_M2     = dependent_var*self.factor_multiplier
                self.BIG_M      = 999999
                self.MASSIVE_M  = 999999
            elif(self.factor_multiplier==3):
                self.BIG_M2     = dependent_var*self.factor_multiplier
                self.BIG_M      = 99999999
                self.MASSIVE_M  = 99999999
            else:
                self.BIG_M2     = dependent_var*self.factor_multiplier
                self.BIG_M      = 999999
                self.MASSIVE_M  = 99999999
        else:
            # Should be much bigger than 550.000, and also significantly bigger than the 
            # total demand used by the building. because: large-550 > 1 * Massive
            # if Massive smaller than the problem becomes an issue
            self.MASSIVE_M  = 99999999
            # For smaller problems this is a good setting.
            self.BIG_M2 = 99999
            self.BIG_M  = 99999
        # Small m is used to put the binary variables
        # in the cost function.
        self.small_M    = 0.000001      
        
        
        super().__init__(excel=excel, 
                         request=request,
                         MAX_ROOF_AREA=MAX_ROOF_AREA,
                         ROOF_AREA_CAP=ROOF_AREA_CAP,
                         big_m=self.BIG_M)
        self.num_panels = NUM_PANELS
        # standard billing period
        self.set_load(pvbuilding.hourly_electricity)
        # This creates an array with the abbrivations of the month ["Jan", "Feb"..]
        self.billing_periods = ['{}'.format(calendar.month_abbr[month_val]) for month_val in range(1,13)]
        # This section creates bins of hours to bin the hourly data into months.
        # starting 0
        self.hours_list = [0]
        self.days_list=[]
        for i in range(11):
            days = monthrange(2011, i+1)[1]
            self.days_list.append(days)
            self.hours_list.append(days*24 + self.hours_list[i])
        # Ending one before 8761
        self.hours_list.append(8761)
        # December has 31 days
        self.days_list.append(31)
        self.resultsFile = resultsFile
        self.all_year_max_demand      = float(pvbuilding.pre_demand_maximum)

        
 
    
    def _add_variables(self):
        super()._add_variables()
        '''
        List of new variables needed for the BC model.
        '''
        # Total imported energy
        self.total_import = RealVariable()
        # Total import cost
        self.total_import_cost = RealVariable()
        # Total exported energy
        self.total_export = RealVariable()
        # Total exported imcome
        self.total_export_income = RealVariable()
        # The daily demand charge
        self.daily_charge_c  = RealVariable()
        # new operating cost > just import-export
        self.operating_cost_new  = RealVariable()
        # Demand charge for every month
        self.demand_charge_c = {period: RealVariable() 
                        for period in self.billing_periods
        }
        # Is the use above 550,000 kWh?
        self.above_550     = BinaryVariable() 
        # is any of the binaries above 35 true
        self.any_above_35  = BinaryVariable() 
        # is any of the binaries above 150 true
        self.any_above_150 = BinaryVariable() 
        # Simple or to make to make switching to the large easier.
        self.a150_or_a550  = BinaryVariable() 
        # Set a number of billing periods
        # Track the maximum energy
        self.max_energy_billing = {period: RealVariable() 
                        for period in self.billing_periods
        }
        # how much energy is imported every month
        self.monthly_import  = {period: RealVariable()
                     for period in self.billing_periods
        }
        # how much energy is exported every month
        self.monthly_export  = {period: RealVariable()
                     for period in self.billing_periods
        }
        # how much does the import cost every month
        self.monthly_import_cost  = {period: RealVariable()
                     for period in self.billing_periods
        }
        # how much can we earn energy month.
        self.monthly_export_income  = {period: RealVariable()
                     for period in self.billing_periods
        }
        # how much is the monthly bill
        self.monthly_bill  = {period: RealVariable()
                     for period in self.billing_periods
        }
        # is the energy use above 35 kW?
        self.above_35  = {period: BinaryVariable()
                     for period in self.billing_periods
        }
        # is the energy use above 150 kW?
        self.above_150 = {period: BinaryVariable()
                     for period in self.billing_periods
        }

    
    
    @constraint()
    def roof_tech_cap_max(self):
        """ 
        Ensure the roof techs are less than 100kW nameplate
        """

        # multiply area-per-unit-capacity and capacity for all roof-techs and sum all of them
        total_roof_cap = sum([self.capacities[tech_name] for tech_name in self.roof_area_per_unit_capacity])
                
        return total_roof_cap*160 <= 100000
    
    @constraint('billing_periods')
    def constrain_max_energy(self,period):
        '''
        This sets an upper bound to the constraint such that there is no possibility for ramping.
        '''
        return self.max_energy_billing[period] <= self.all_year_max_demand+0.1
    
    
    ######################## DAILY CHARGE  #######################################

    @constraint()
    def daily_c_upper_small(self):
        """
        small -> 365*.3645
        rest -> above 35kW is TRUE - > 365*.2673
        """
     
        return self.daily_charge_c <= .3645 *365 + self.BIG_M2*self.any_above_35

    @constraint()
    def daily_c_lower_small(self):
        """
        small -> 365*.3645
        rest -> above 35kW is TRUE - > 365*.2673
        """
        return self.daily_charge_c >= .3645*365 - self.BIG_M2* self.any_above_35 - .3645*365 * (1-self.is_installed['Grid'])
    
    @constraint()
    def daily_c_upper_medlar(self):
        """
        small -> 365*.3645
        rest -> above 35kW is TRUE - > 365*.2673
        """
     
        return self.daily_charge_c <= .2673*365 + self.BIG_M2 * (1-self.any_above_35)

    @constraint()
    def daily_c_lower_medlar(self):
        """
        small -> 365*.3645
        rest -> above 35kW is TRUE - > 365*.2673
        """
        return self.daily_charge_c >= .2673*365 - self.BIG_M2 * (1-self.any_above_35) - .2673*365* (1-self.is_installed['Grid']) 
    
    @constraint('billing_periods')
    def monthly_bill_upper(self,period):
        """
        hello
        """
        
        # Daily charge
        # find the number of days from the days list
        # multiply by the charge rate
        i=self.billing_periods.index(period)
        
        C_D = self.days_list[i] * self.daily_charge_c / 365
        C_E = self.monthly_import_cost[period] - self.monthly_export_income[period]
        C_d = self.demand_charge_c[period]
        return self.monthly_bill[period] >= C_E + C_D + C_d

    @constraint('billing_periods')
    def monthly_bill_lower(self,period):
        """
        hello
        """
        
        # Daily charge
        # find the number of days from the days list
        # multiply by the charge rate
        i=self.billing_periods.index(period)
        
        C_D = self.days_list[i] * self.daily_charge_c / 365
        C_E = self.monthly_import_cost[period] - self.monthly_export_income[period]
        C_d = self.demand_charge_c[period]
        return self.monthly_bill[period] <= C_E + C_D + C_d

    
    ######################## SMALL  #######################################
    ######################## IMPORT #######################################

    @constraint('billing_periods')
    def small_operating_import_lower(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.1253
        add  = - self.BIG_M2*(self.any_above_35 +self.a150_or_a550)
        return self.monthly_import_cost[period] >= c_bc*self.monthly_import[period] + add 

    @constraint('billing_periods')
    def small_operating_import_upper(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.1253
        add  = self.BIG_M2*(self.any_above_35 +self.a150_or_a550)
        return self.monthly_import_cost[period] <= c_bc*self.monthly_import[period] + add     

    ######################## EXPORT #######################################

    @constraint('billing_periods')
    def small_operating_export_lower(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.1253
        add  = - self.BIG_M2*(self.any_above_35 +self.a150_or_a550)
        return self.monthly_export_income[period] >= c_bc*self.monthly_export[period] + add 

    @constraint('billing_periods')
    def small_operating_export_upper(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.1253
        add  = self.BIG_M2*(self.any_above_35 +self.a150_or_a550)
        return self.monthly_export_income[period] <= c_bc*self.monthly_export[period] + add   
    #######################################################################
    ######################## MEDIUM  #######################################
    ######################## IMPORT #######################################

    @constraint('billing_periods')
    def med_operating_import_lower(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.0986
        add  = - self.BIG_M2*(1-self.any_above_35 +self.a150_or_a550)
        return self.monthly_import_cost[period] >= c_bc*self.monthly_import[period] + add 

    @constraint('billing_periods')
    def med_operating_import_upper(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.0986
        add  = self.BIG_M2*(1-self.any_above_35 +self.a150_or_a550)
        return self.monthly_import_cost[period] <= c_bc*self.monthly_import[period] + add     

    ######################## EXPORT #######################################

    @constraint('billing_periods')
    def med_operating_export_lower(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.0986
        add  = - self.BIG_M2*(1-self.any_above_35 +self.a150_or_a550)
        return self.monthly_export_income[period] >= c_bc*self.monthly_export[period] + add 

    @constraint('billing_periods')
    def med_operating_export_upper(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.0986
        add  = self.BIG_M2*(1-self.any_above_35 +self.a150_or_a550)
        return self.monthly_export_income[period] <= c_bc*self.monthly_export[period] + add   
    #######################################################################    
    ######################## LARGE   #######################################
    ######################## IMPORT #######################################

    @constraint('billing_periods')
    def large_operating_import_lower(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.0606
        add  = - self.BIG_M2*(1-self.a150_or_a550)
        return self.monthly_import_cost[period] >= c_bc*self.monthly_import[period] + add 

    @constraint('billing_periods')
    def large_operating_import_upper(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.0606
        add  = self.BIG_M2*(1-self.a150_or_a550)
        return self.monthly_import_cost[period] <= c_bc*self.monthly_import[period] + add     

    ######################## EXPORT #######################################

    @constraint('billing_periods')
    def large_operating_export_lower(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.0606
        add  = - self.BIG_M2*(1-self.a150_or_a550)
        return self.monthly_export_income[period] >= c_bc*self.monthly_export[period] + add 

    @constraint('billing_periods')
    def large_operating_export_upper(self,period):
        """
        if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
        then use the small charge
        """
        c_bc = 0.0606
        add  = self.BIG_M2*(1-self.a150_or_a550)
        return self.monthly_export_income[period] <= c_bc*self.monthly_export[period] + add   
    #######################################################################
    


    ######################## new total cost  #############################
    @constraint()
    def calc_total_cost(self):
        """
        Add additional costs to the standard ehub cost calculation
        """
        parent_constraint = super().calc_total_cost()
        old_total_cost = parent_constraint.rhs
        '''
        Special grid costs
        
        Demand Charge:
            max_energy_billing: tracks the maximum energy used during a billing period
            above_35: is a bool that tracks if max_enregy_billing is above 35kW
            demand_charge_c: is the cost associated with the demand charge
        Daily Charge:
            daily: is the yearly about due to daily costs
        '''
        # Start the grid cost counting
        grid_cost = 0
        small_M = 0.00001
        # These following small M's help the problem not switch
        for period in self.billing_periods:
            grid_cost += self.max_energy_billing[period]*small_M
            grid_cost += self.above_35[period]*small_M
            grid_cost += self.above_150[period]*small_M
        grid_cost += self.above_550*small_M 
        grid_cost += self.any_above_35*small_M
        grid_cost += self.any_above_150*small_M
        grid_cost += self.a150_or_a550*small_M
        
        # actual grid cost is the operating cost
        # grid_cost += self.operating_cost_new
        # with the daily charge
        #grid_cost += self.daily_charge_c
        # add on the the demand charges.
        for period in self.billing_periods:
            grid_cost += self.monthly_bill[period]
            #grid_cost += self.demand_charge_c[period]
        
        return self.total_cost == old_total_cost + grid_cost
    
    ######################## TRACKING MAX  #################################
    
    
    @constraint('time','billing_periods')
    def max_track_constraint(self,t,period):
        '''
        This function makes sure to track the maxumim kW used during each billing period.
        billing_period =  12 Translates to every month.
        '''
        i=self.billing_periods.index(period)
        if ((t>=self.hours_list[i]) & (t<self.hours_list[i+1])):
            segment = self.energy_imported[t]['Grid']
            return self.max_energy_billing[period] >= segment
        else:
            return self.max_energy_billing[period] >= 0
        

    
   
    ######################## DEMAND CHARGE  #################################
    ######################## SMALL    #######################################

    @constraint('billing_periods')
    def small_demand_charge_constraint(self, period):
        """
        Default small - demand charge is zero.
        """
        return self.demand_charge_c[period] >=  0
    
    
    
    ######################## MEDIUM   #######################################
   
    @constraint('billing_periods')
    def medium_upper(self, period):
        """
        medium => above 35 is TRUE, above 150 is FALSE and above 550 is FALSE
        """
        add =  - self.BIG_M2*((1-self.any_above_35)+self.a150_or_a550)
        return self.demand_charge_c[period] >=  self.max_energy_billing[period]*5.42 + add
    
    @constraint('billing_periods')
    def medium_lower(self, period):
        """
        medium => above 35 is TRUE, above 150 is FALSE and above 550 is FALSE
        """
        add =    self.BIG_M2*((1-self.any_above_35)+self.a150_or_a550)
        return self.demand_charge_c[period] <=  self.max_energy_billing[period]*5.42 + add
    
    
    ######################## LARGE   #######################################
    @constraint('billing_periods')
    def large_upper(self, period):
        """
        large => above 35 is TRUE, above 150 is TRUE or above 550 is TRUE
        """
        add = - self.BIG_M2*(1-self.a150_or_a550) 
        return self.demand_charge_c[period] >=  self.max_energy_billing[period]*12.34 + add
    
    @constraint('billing_periods')
    def large_lower(self, period):
        """
        large => above 150 is TRUE or above 550 is TRUE
        """
        add = self.BIG_M2*(1-self.a150_or_a550)
        return self.demand_charge_c[period] <=  self.max_energy_billing[period]*12.34 + add
    

    ###########################################################################
    @constraint()
    def track_or_constraint_lower(self):
        """
        See if any of the periods go beyond 150 kW or above 550kWh
        """
        return self.a150_or_a550 <= self.any_above_150 + self.above_550 
    
    @constraint()
    def track_or_constraint_upper(self):
        """
        See if any of the periods go beyond 150 kW or above 550kWh
        """

        return self.a150_or_a550 >= self.any_above_150 
    @constraint()
    def track_or_constraint_upper2(self):
        """
        See if any of the periods go beyond 150 kW or above 550kWh
        """

        return self.a150_or_a550 >= self.above_550     
    
    
    
    
    @constraint()
    def track_any_above_35_constraint_upper(self):
        """
        See if any of the periods go beyond 35 kW
        """
        rhs = self.any_above_35
        lhs = 0
        for period in self.billing_periods:
            lhs += self.above_35[period]
        return lhs >= rhs 
    
    @constraint()
    def track_any_above_35_constraint_lower(self):
        """
        See if any of the periods go beyond 35 kW
        """
        rhs = self.BIG_M2 *self.any_above_35
        lhs = 0
        for period in self.billing_periods:
            lhs += self.above_35[period]
        return lhs <= rhs 

    @constraint()
    def track_any_above_150_constraint_upper(self):
        """
        See if any of the periods go beyond 150 kW
        """
        rhs = self.any_above_150
        lhs = 0
        for period in self.billing_periods:
            lhs += self.above_150[period]
        return lhs >= rhs 

    @constraint()
    def track_any_above_150_constraint_lower(self):
        """
        See if any of the periods go beyond 150 kW
        """
        rhs = self.BIG_M2 *self.any_above_150
        lhs = 0
        for period in self.billing_periods:
            lhs += self.above_150[period]
        return lhs <= rhs 
    
    
    
    @constraint('billing_periods')
    def track_above_35_constraint_upper(self, period):
        """
        Set binary to 1 if max val is larger than 35
        Args:
            period: slice of time
        """
        lhs = self.max_energy_billing[period]-35
        rhs = self.BIG_M2 * self.above_35[period]
        return lhs    <= rhs +self.small_M 

    @constraint('billing_periods')
    def track_above_35_constraint_lower(self, period):
        """
        Set binary to 1 if max val is larger than 35
        Args:
            period: slice of time
        """
        lhs = self.max_energy_billing[period]-35
        rhs = -self.BIG_M2 * (1-self.above_35[period])
        return lhs >= rhs + self.small_M     

    
    @constraint('billing_periods')
    def track_above_150_constraint_upper(self, period):
        """
        Set binary to 1 if max val is larger than 150
        Args:
            period: slice of time
        """
        lhs = self.max_energy_billing[period]-150
        rhs = self.BIG_M2 * self.above_150[period]
        return lhs <= rhs +self.small_M     
    
    @constraint('billing_periods')
    def track_above_150_constraint_lower(self, period):
        """
        Set binary to 1 if max val is larger than 150
        Args:
            period: slice of time
        """
        lhs = self.max_energy_billing[period]-150
        rhs = -self.BIG_M2 * (1-self.above_150[period])
        return lhs >= rhs + self.small_M    
    
    
    @constraint()
    def track_above_550_constraint_upper(self):
        """
        Set binary to 1 if total imported from the grid
        is larger than 550,000 kWh.
        """
        lhs = self.total_import-550000
        rhs = self.MASSIVE_M * self.above_550
        return lhs <= rhs+self.small_M    

    @constraint()
    def track_above_550_constraint_lower(self):
        """
        Set binary to 1 if total imported from the grid
        is larger than 550,000 kWh.
        """
        lhs = self.total_import-550000
        rhs = -self.MASSIVE_M * (1-self.above_550)
        return lhs >= rhs+self.small_M 

  
    @constraint()
    def total_import_value_constraint(self):
        """
        This constraints tracks the total imports
        """
        imports = 0
        for t in self.time:
            imports += self.energy_imported[t]['Grid']
        return self.total_import == imports
    
    @constraint()
    def total_export_value_constraint(self):
        """
        This constraints tracks the total exports from the panels which is funneled through 
        GreenElec stream. 
        """
        exports = 0
        for t in self.time:
            exports += self.energy_exported[t]['GreenElec'] 
        return self.total_export == exports

    
    @constraint('billing_periods')
    def monthly_import_constraint(self,period):
        """
        This constraints tracks the monthly imports from the grid
        """
        i=self.billing_periods.index(period)

        imports=[self.energy_imported[t]['Grid'] 
                 for t in self.time if ((t>=self.hours_list[i]) & (t<self.hours_list[i+1]))]
        
        return self.monthly_import[period] == sum(imports)
    
    @constraint('billing_periods')
    def monthly_export_constraint(self,period):
        """
        This constraint tracks the monthly exports from the green electricity and the electricity.
        """
        i=self.billing_periods.index(period)

        export=[self.energy_exported[t]['GreenElec'] 
                for t in self.time if ((t>=self.hours_list[i]) & (t<self.hours_list[i+1]))]
        
        return self.monthly_export[period] == sum(export)
    
    
   
    @constraint()
    def net_metering_constraint(self):
        """
        Netmetering constraint
        
        I've added a 0.1 due to the rounding error of the solver 
        we something get a small amount of more exported than 
        imported. The 0.1 adds a little bit that does not give 
        us this error.
        """
        return self.total_export+0.1 <= self.total_import
    
        
    def eval_model(self,values,price,pvpanel):
        '''
        This is the evaluator function that run the EHub model based on a set 
        of values that corrospond to the panel orientations. 
        param: values   - set orientations for five panels
        param: price    - the price of the pv panel
        param: my_model - The EHub model
        param: pvpanel  - The pv panel setup on the building
        '''
        print("Start Evaluation")
        # Determine the number of panels used.
        self.num_panels = len(self.TIME_SERIES.keys()) - 1 
        
        azi  = values[0:self.num_panels]
        tilt = values[self.num_panels:self.num_panels*2]
        gcr  = values[self.num_panels*2:self.num_panels*3]
        
        # subtract Elec load the rest of the timeseries are irradiances
        area_per_panel = [pvpanel.pp['pvrow_depth']*pvpanel.pp['pvrow_width']/gcr[i] for i in range(self.num_panels)]
        self.roof_area_per_unit_capacity = {"PV{}".format(i):area_per_panel[i] for i in range(self.num_panels)}
        t1 = time()
        # Replace them with the irradiance from pvfactors
        for i in range(self.num_panels):
            # here we replace model's old PV data with the new
            print('Compiling and setting: Irra{} based on:\n \
                                          an Azimuth of {:.0f}\n \
                                          a tilt of {:.1f}\n \
                                          and a gcr of {:.3f}'.format(i,azi[i],tilt[i],gcr[i]))
            IRRADIANCE = pvpanel.irradiance(azi[i],tilt[i],gcr[i])
            self.set_irr(i,IRRADIANCE)
            # Here we set the linear capital cost of the panels
            self.set_lin_cap_cost(i, price)
        t2 = time()
        print('Generated the panel irradiance in {:3.0f}s'.format(t2-t1))
        t1 = time()
        print('Starting recompile')
        self.recompile()
        t2 = time()
        print('Recompiled the model in {:3.0f}s'.format(t2-t1))
        t1 = time()
        print('Starting solver')
        # Now we solve the model and get back our results
        results = self.solve()
        self.solveResults = results
        t2 = time()
        # Total Cost of the System
        Cost            = self.solveResults['solution']['total_cost']
        print('Solved the model in {:.0f}s'.format(t2-t1))
        panel_relevance = [self.solveResults['solution']['capacity_tech']['PV{}'.format(i)] 
                               for i in range(self.num_panels)]
        print(panel_relevance)
        if (Cost!=0):
            # helper function
            def sum_results(key,field):
                length=len(self.solveResults['solution'][key])
                sum_total=0
                for i in range(length):
                    sum_total+=self.solveResults['solution'][key][i][field]
                return sum_total
            energy_load = sum(self.solveResults['solution']['LOADS']['Elec'].values())
            monthly_import_dict = self.solveResults['solution']['monthly_import_cost']
            monthly_export_dict = self.solveResults['solution']['monthly_export_income']
            energy_cost = sum([monthly_import_dict[period] - monthly_export_dict[period] for period in self.billing_periods] )
            energy_impo = self.solveResults['solution']['total_import']
            print(Cost)
            totaldemandcost = sum(self.solveResults['solution']['demand_charge_c'].values())
            print(totaldemandcost)
            daily_cost      = self.solveResults['solution']['daily_charge_c']
            print(daily_cost)
            output_excel(results['solution'], self.resultsFile, 
                         time_steps=len(self.time), sheets=['other', 'capacity_tech'])

            cost_break = {'Levelized Costs':Cost-energy_cost-totaldemandcost-daily_cost,
                          'Energy Charge':energy_cost,
                          'Demand Charge':totaldemandcost,
                          'Daily Charge' :daily_cost}

            energy_produced = sum(results['solution']['energy_input']['Invertor']) 

            self.Orientations = values
            self.totCost      = Cost
            self.totCapacity  = sum(panel_relevance)
            self.panelCap     = panel_relevance
            self.costBreak    = cost_break
            self.energyProd   = energy_produced
            self.energyLoad   = energy_load
            self.energyIm     = energy_impo
            self.roof_area_used  = self.solveResults['solution']['total_roof_area_used']
            self.make_pie_chart()
            self.roof_percentage()
        else:
            print("Can not solve this model correctly.")
            self.Orientations = values
            self.totCost      = 9*10^10
            self.totCapacity  = 0
            self.panelCap     = panel_relevance
            self.costBreak    = [0,0,0]
            self.energyProd   = 9*10^10
            self.energyLoad   = 9*10^10
            self.energyIm     = 9*10^10
            self.roof_area_used  = 0

        
        
        
# class RoofPlusModelBC(EHubModel):
#     """
#     We fused both models into one to speed up recompile
#     """
    
#     def __init__(self, *, excel=None, 
#                  request=None,
#                  max_carbon=0,
#                  pvbuilding=None,
#                  ROOF_AREA_CAP=0,
#                  resultsFile=None,
#                  NUM_PANELS=3,
#                  factor_multiplier=1.5): # 1.5 is based on trial and error.
#         self.factor_multiplier = factor_multiplier
#         self.roof_area_per_unit_capacity = ROOF_AREA_CAP
#         MAX_ROOF_AREA = pvbuilding.roof_area
#         self.service_rate = pvbuilding.check_bc_rate()

#         # Should be much bigger than 550.000, and also significantly bigger than the 
#         # total demand used by the building. because: large-550 > 1 * Massive
#         # if Massive smaller than the problem becomes an issue
#         self.MASSIVE_M  = 9999999
#         # big M should be bigger than 
#         # highest demand charge total.
#         # We found that the model does wierd things
#         # when 
#         dependent_var = pvbuilding.max_demand()
#         if (dependent_var > 99999):
#             # We use big_m in our code to make a couple binaries work
#             # They pivot around this depedent var. 
#             self.BIG_M2      = dependent_var*self.factor_multiplier # 1.5 is based on trial and error.
#             self.BIG_M       = 99999 # BIG_M is the big_m for the orginal code.
#         else:
#             self.BIG_M2 = 99999
#             self.BIG_M  = 99999
#         # Small m is used to put the binary variables
#         # in the cost function.
#         self.small_M    = 0.00001      
        
#         super().__init__(excel=excel, request=request,big_m=self.BIG_M)
#         self.MAX_ROOF_AREA  = MAX_ROOF_AREA
#         self.roof_area_per_unit_capacity = ROOF_AREA_CAP
        
#         self.num_panels = NUM_PANELS
#         # standard billing period
#         self.set_load(pvbuilding.hourly_electricity)
#         # This creates an array with the abbrivations of the month ["Jan", "Feb"..]
#         self.billing_periods = ['{}'.format(calendar.month_abbr[month_val]) for month_val in range(1,13)]
#         # This section creates bins of hours to bin the hourly data into months.
#         # starting 0
#         self.hours_list = [0]
#         self.days_list=[]
#         for i in range(11):
#             days = monthrange(2011, i+1)[1]
#             self.days_list.append(days)
#             self.hours_list.append(days*24 + self.hours_list[i])
#         # Ending one before 8761
#         self.hours_list.append(8761)
#         # December has 31 days
#         self.days_list.append(31)
#         self.resultsFile = resultsFile
#         self.all_year_max_demand      = float(pvbuilding.pre_demand_maximum)

        
 
    
#     def _add_variables(self):
#         super()._add_variables()
#         '''
#         List of new variables needed for the BC model.
#         '''
#         # Total imported energy
#         self.total_import = RealVariable()
#         # Total import cost
#         self.total_import_cost = RealVariable()
#         # Total exported energy
#         self.total_export = RealVariable()
#         # Total exported imcome
#         self.total_export_income = RealVariable()
#         # The daily demand charge
#         self.daily_charge_c  = RealVariable()
#         # new operating cost > just import-export
#         self.operating_cost_new  = RealVariable()
#         # Demand charge for every month
#         self.demand_charge_c = {period: RealVariable() 
#                         for period in self.billing_periods
#         }
#         # Is the use above 550,000 kWh?
#         self.above_550     = BinaryVariable() 
#         # is any of the binaries above 35 true
#         self.any_above_35  = BinaryVariable() 
#         # is any of the binaries above 150 true
#         self.any_above_150 = BinaryVariable() 
#         # Simple or to make to make switching to the large easier.
#         self.a150_or_a550  = BinaryVariable() 
#         # Set a number of billing periods
#         # Track the maximum energy
#         self.max_energy_billing = {period: RealVariable() 
#                         for period in self.billing_periods
#         }
#         # how much energy is imported every month
#         self.monthly_import  = {period: RealVariable()
#                      for period in self.billing_periods
#         }
#         # how much energy is exported every month
#         self.monthly_export  = {period: RealVariable()
#                      for period in self.billing_periods
#         }
#         # how much does the import cost every month
#         self.monthly_import_cost  = {period: RealVariable()
#                      for period in self.billing_periods
#         }
#         # how much can we earn energy month.
#         self.monthly_export_income  = {period: RealVariable()
#                      for period in self.billing_periods
#         }
#         # how much is the monthly bill
#         self.monthly_bill  = {period: RealVariable()
#                      for period in self.billing_periods
#         }
#         # is the energy use above 35 kW?
#         self.above_35  = {period: BinaryVariable()
#                      for period in self.billing_periods
#         }
#         # is the energy use above 150 kW?
#         self.above_150 = {period: BinaryVariable()
#                      for period in self.billing_periods
#         }
#         # This is the total roof area from the building
#         # we have this real variable here such that it 
#         # shows up on the results
#         self.total_roof_area      = RealVariable()
#         # This is the total roof area used by the solar
#         # panels. 
#         self.total_roof_area_used = RealVariable()

    
#     @constraint()
#     def roof_tech_area_definition(self):
#         """ 
#         This calculates the roof area and adds it to the output file
#         """

#         # multiply area-per-unit-capacity and capacity for all roof-techs and sum all of them
#         total_roof_area_used = sum((self.capacities[tech_name])*(self.roof_area_per_unit_capacity[tech_name])
#                               for tech_name in self.roof_area_per_unit_capacity)
                
#         return self.total_roof_area_used == total_roof_area_used      
    
#     @constraint()
#     def total_roof_area_definition(self):
#         """ 
#         This adds the total roof area to the output file 
#         """
#         return self.total_roof_area == float(self.MAX_ROOF_AREA)
    
#     @constraint()
#     def roof_tech_area_below_max(self):
#         """ 
#         Ensure the roof techs are taking up less area than there is roof.
#         """

#         # multiply area-per-unit-capacity and capacity for all roof-techs and sum all of them
#         total_roof_area_used = sum((self.capacities[tech_name])*(self.roof_area_per_unit_capacity[tech_name])
#                               for tech_name in self.roof_area_per_unit_capacity)
                
#         return total_roof_area_used <= float(self.MAX_ROOF_AREA)
    
#     @constraint()
#     def roof_tech_cap_max(self):
#         """ 
#         Ensure the roof techs are less than 100kW nameplate
#         """

#         # multiply area-per-unit-capacity and capacity for all roof-techs and sum all of them
#         total_roof_cap = sum([self.capacities[tech_name] for tech_name in self.roof_area_per_unit_capacity])
                
#         return total_roof_cap*160 <= 100000
    
#     @constraint('billing_periods')
#     def constrain_max_energy(self,period):
#         '''
#         This sets an upper bound to the constraint such that there is no possibility for ramping.
#         '''
#         return self.max_energy_billing[period] <= self.all_year_max_demand
    
    
#     ######################## DAILY CHARGE  #######################################

#     @constraint()
#     def daily_c_upper_small(self):
#         """
#         small -> 365*.3645
#         rest -> above 35kW is TRUE - > 365*.2673
#         """
     
#         return self.daily_charge_c <= .3645 *365 + self.BIG_M2*self.any_above_35

#     @constraint()
#     def daily_c_lower_small(self):
#         """
#         small -> 365*.3645
#         rest -> above 35kW is TRUE - > 365*.2673
#         """
#         return self.daily_charge_c >= .3645*365 - self.BIG_M2* self.any_above_35 - .3645*365 * (1-self.is_installed['Grid'])
    
#     @constraint()
#     def daily_c_upper_medlar(self):
#         """
#         small -> 365*.3645
#         rest -> above 35kW is TRUE - > 365*.2673
#         """
     
#         return self.daily_charge_c <= .2673*365 + self.BIG_M2 * (1-self.any_above_35)

#     @constraint()
#     def daily_c_lower_medlar(self):
#         """
#         small -> 365*.3645
#         rest -> above 35kW is TRUE - > 365*.2673
#         """
#         return self.daily_charge_c >= .2673*365 - self.BIG_M2 * (1-self.any_above_35) - .2673*365* (1-self.is_installed['Grid']) 
    
#     @constraint('billing_periods')
#     def monthly_bill_upper(self,period):
#         """
#         hello
#         """
        
#         # Daily charge
#         # find the number of days from the days list
#         # multiply by the charge rate
#         i=self.billing_periods.index(period)
        
#         C_D = self.days_list[i] * self.daily_charge_c / 365
#         C_E = self.monthly_import_cost[period] - self.monthly_export_income[period]
#         C_d = self.demand_charge_c[period]
#         return self.monthly_bill[period] >= C_E + C_D + C_d

#     @constraint('billing_periods')
#     def monthly_bill_lower(self,period):
#         """
#         hello
#         """
        
#         # Daily charge
#         # find the number of days from the days list
#         # multiply by the charge rate
#         i=self.billing_periods.index(period)
        
#         C_D = self.days_list[i] * self.daily_charge_c / 365
#         C_E = self.monthly_import_cost[period] - self.monthly_export_income[period]
#         C_d = self.demand_charge_c[period]
#         return self.monthly_bill[period] <= C_E + C_D + C_d

    
#     ######################## SMALL  #######################################
#     ######################## IMPORT #######################################

#     @constraint('billing_periods')
#     def small_operating_import_lower(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.1253
#         add  = - self.BIG_M2*(self.any_above_35 +self.a150_or_a550)
#         return self.monthly_import_cost[period] >= c_bc*self.monthly_import[period] + add 

#     @constraint('billing_periods')
#     def small_operating_import_upper(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.1253
#         add  = self.BIG_M2*(self.any_above_35 +self.a150_or_a550)
#         return self.monthly_import_cost[period] <= c_bc*self.monthly_import[period] + add     

#     ######################## EXPORT #######################################

#     @constraint('billing_periods')
#     def small_operating_export_lower(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.1253
#         add  = - self.BIG_M2*(self.any_above_35 +self.a150_or_a550)
#         return self.monthly_export_income[period] >= c_bc*self.monthly_export[period] + add 

#     @constraint('billing_periods')
#     def small_operating_export_upper(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.1253
#         add  = self.BIG_M2*(self.any_above_35 +self.a150_or_a550)
#         return self.monthly_export_income[period] <= c_bc*self.monthly_export[period] + add   
#     #######################################################################
#     ######################## MEDIUM  #######################################
#     ######################## IMPORT #######################################

#     @constraint('billing_periods')
#     def med_operating_import_lower(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.0986
#         add  = - self.BIG_M2*(1-self.any_above_35 +self.a150_or_a550)
#         return self.monthly_import_cost[period] >= c_bc*self.monthly_import[period] + add 

#     @constraint('billing_periods')
#     def med_operating_import_upper(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.0986
#         add  = self.BIG_M2*(1-self.any_above_35 +self.a150_or_a550)
#         return self.monthly_import_cost[period] <= c_bc*self.monthly_import[period] + add     

#     ######################## EXPORT #######################################

#     @constraint('billing_periods')
#     def med_operating_export_lower(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.0986
#         add  = - self.BIG_M2*(1-self.any_above_35 +self.a150_or_a550)
#         return self.monthly_export_income[period] >= c_bc*self.monthly_export[period] + add 

#     @constraint('billing_periods')
#     def med_operating_export_upper(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.0986
#         add  = self.BIG_M2*(1-self.any_above_35 +self.a150_or_a550)
#         return self.monthly_export_income[period] <= c_bc*self.monthly_export[period] + add   
#     #######################################################################    
#     ######################## LARGE   #######################################
#     ######################## IMPORT #######################################

#     @constraint('billing_periods')
#     def large_operating_import_lower(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.0606
#         add  = - self.BIG_M2*(1-self.a150_or_a550)
#         return self.monthly_import_cost[period] >= c_bc*self.monthly_import[period] + add 

#     @constraint('billing_periods')
#     def large_operating_import_upper(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.0606
#         add  = self.BIG_M2*(1-self.a150_or_a550)
#         return self.monthly_import_cost[period] <= c_bc*self.monthly_import[period] + add     

#     ######################## EXPORT #######################################

#     @constraint('billing_periods')
#     def large_operating_export_lower(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.0606
#         add  = - self.BIG_M2*(1-self.a150_or_a550)
#         return self.monthly_export_income[period] >= c_bc*self.monthly_export[period] + add 

#     @constraint('billing_periods')
#     def large_operating_export_upper(self,period):
#         """
#         if above 35kW is FALSE and or(above 150kW, above 550kW) is FALSE
#         then use the small charge
#         """
#         c_bc = 0.0606
#         add  = self.BIG_M2*(1-self.a150_or_a550)
#         return self.monthly_export_income[period] <= c_bc*self.monthly_export[period] + add   
#     #######################################################################
    


#     ######################## new total cost  #############################
#     @constraint()
#     def calc_total_cost(self):
#         """
#         Add additional costs to the standard ehub cost calculation
#         """
#         parent_constraint = super().calc_total_cost()
#         old_total_cost = parent_constraint.rhs
#         '''
#         Special grid costs
        
#         Demand Charge:
#             max_energy_billing: tracks the maximum energy used during a billing period
#             above_35: is a bool that tracks if max_enregy_billing is above 35kW
#             demand_charge_c: is the cost associated with the demand charge
#         Daily Charge:
#             daily: is the yearly about due to daily costs
#         '''
#         # Start the grid cost counting
#         grid_cost = 0
#         small_M = 0.00001
#         # These following small M's help the problem not switch
#         for period in self.billing_periods:
#             grid_cost += self.max_energy_billing[period]*small_M
#             grid_cost += self.above_35[period]*small_M
#             grid_cost += self.above_150[period]*small_M
#         grid_cost += self.above_550*small_M 
#         grid_cost += self.any_above_35*small_M
#         grid_cost += self.any_above_150*small_M
#         grid_cost += self.a150_or_a550*small_M
        
#         # actual grid cost is the operating cost
#         # grid_cost += self.operating_cost_new
#         # with the daily charge
#         #grid_cost += self.daily_charge_c
#         # add on the the demand charges.
#         for period in self.billing_periods:
#             grid_cost += self.monthly_bill[period]
#             #grid_cost += self.demand_charge_c[period]
        
#         return self.total_cost == old_total_cost + grid_cost
    
#     ######################## TRACKING MAX  #################################
    
    
#     @constraint('time','billing_periods')
#     def max_track_constraint(self,t,period):
#         '''
#         This function makes sure to track the maxumim kW used during each billing period.
#         billing_period =  12 Translates to every month.
#         '''
#         i=self.billing_periods.index(period)
#         if ((t>=self.hours_list[i]) & (t<self.hours_list[i+1])):
#             segment = self.energy_imported[t]['Grid']
#             return self.max_energy_billing[period] >= segment
#         else:
#             return self.max_energy_billing[period] >= 0
        

    
   
#     ######################## DEMAND CHARGE  #################################
#     ######################## SMALL    #######################################

#     @constraint('billing_periods')
#     def small_demand_charge_constraint(self, period):
#         """
#         Default small - demand charge is zero.
#         """
#         return self.demand_charge_c[period] >=  0
    
    
    
#     ######################## MEDIUM   #######################################
   
#     @constraint('billing_periods')
#     def medium_upper(self, period):
#         """
#         medium => above 35 is TRUE, above 150 is FALSE and above 550 is FALSE
#         """
#         add =  - self.BIG_M2*((1-self.any_above_35)+self.a150_or_a550)
#         return self.demand_charge_c[period] >=  self.max_energy_billing[period]*5.42 + add
    
#     @constraint('billing_periods')
#     def medium_lower(self, period):
#         """
#         medium => above 35 is TRUE, above 150 is FALSE and above 550 is FALSE
#         """
#         add =    self.BIG_M2*((1-self.any_above_35)+self.a150_or_a550)
#         return self.demand_charge_c[period] <=  self.max_energy_billing[period]*5.42 + add
    
    
#     ######################## LARGE   #######################################
#     @constraint('billing_periods')
#     def large_upper(self, period):
#         """
#         large => above 35 is TRUE, above 150 is TRUE or above 550 is TRUE
#         """
#         add = - self.BIG_M2*(1-self.a150_or_a550) 
#         return self.demand_charge_c[period] >=  self.max_energy_billing[period]*12.34 + add
    
#     @constraint('billing_periods')
#     def large_lower(self, period):
#         """
#         large => above 150 is TRUE or above 550 is TRUE
#         """
#         add = self.BIG_M2*(1-self.a150_or_a550)
#         return self.demand_charge_c[period] <=  self.max_energy_billing[period]*12.34 + add
    

#     ###########################################################################
#     @constraint()
#     def track_or_constraint_lower(self):
#         """
#         See if any of the periods go beyond 150 kW or above 550kWh
#         """
#         return self.a150_or_a550 <= self.any_above_150 + self.above_550 
    
#     @constraint()
#     def track_or_constraint_upper(self):
#         """
#         See if any of the periods go beyond 150 kW or above 550kWh
#         """

#         return self.a150_or_a550 >= self.any_above_150 
#     @constraint()
#     def track_or_constraint_upper2(self):
#         """
#         See if any of the periods go beyond 150 kW or above 550kWh
#         """

#         return self.a150_or_a550 >= self.above_550     
    
    
    
    
#     @constraint()
#     def track_any_above_35_constraint_upper(self):
#         """
#         See if any of the periods go beyond 35 kW
#         """
#         rhs = self.any_above_35
#         lhs = 0
#         for period in self.billing_periods:
#             lhs += self.above_35[period]
#         return lhs >= rhs 
    
#     @constraint()
#     def track_any_above_35_constraint_lower(self):
#         """
#         See if any of the periods go beyond 35 kW
#         """
#         rhs = self.BIG_M2 *self.any_above_35
#         lhs = 0
#         for period in self.billing_periods:
#             lhs += self.above_35[period]
#         return lhs <= rhs 

#     @constraint()
#     def track_any_above_150_constraint_upper(self):
#         """
#         See if any of the periods go beyond 150 kW
#         """
#         rhs = self.any_above_150
#         lhs = 0
#         for period in self.billing_periods:
#             lhs += self.above_150[period]
#         return lhs >= rhs 

#     @constraint()
#     def track_any_above_150_constraint_lower(self):
#         """
#         See if any of the periods go beyond 150 kW
#         """
#         rhs = self.BIG_M2 *self.any_above_150
#         lhs = 0
#         for period in self.billing_periods:
#             lhs += self.above_150[period]
#         return lhs <= rhs 
    
    
    
#     @constraint('billing_periods')
#     def track_above_35_constraint_upper(self, period):
#         """
#         Set binary to 1 if max val is larger than 35
#         Args:
#             period: slice of time
#         """
#         lhs = self.max_energy_billing[period]-35
#         rhs = self.BIG_M2 * self.above_35[period]
#         return lhs    <= rhs 

#     @constraint('billing_periods')
#     def track_above_35_constraint_lower(self, period):
#         """
#         Set binary to 1 if max val is larger than 35
#         Args:
#             period: slice of time
#         """
#         lhs = self.max_energy_billing[period]-35
#         rhs = -self.BIG_M2 * (1-self.above_35[period])
#         return lhs >= rhs + self.small_M     

    
#     @constraint('billing_periods')
#     def track_above_150_constraint_upper(self, period):
#         """
#         Set binary to 1 if max val is larger than 150
#         Args:
#             period: slice of time
#         """
#         lhs = self.max_energy_billing[period]-150
#         rhs = self.BIG_M2 * self.above_150[period]
#         return lhs <= rhs     
    
#     @constraint('billing_periods')
#     def track_above_150_constraint_lower(self, period):
#         """
#         Set binary to 1 if max val is larger than 150
#         Args:
#             period: slice of time
#         """
#         lhs = self.max_energy_billing[period]-150
#         rhs = -self.BIG_M2 * (1-self.above_150[period])
#         return lhs >= rhs + self.small_M    
    
    
#     @constraint()
#     def track_above_550_constraint_upper(self):
#         """
#         Set binary to 1 if total imported from the grid
#         is larger than 550,000 kWh.
#         """
#         lhs = self.total_import-550000
#         rhs = self.MASSIVE_M * self.above_550
#         return lhs <= rhs   

#     @constraint()
#     def track_above_550_constraint_lower(self):
#         """
#         Set binary to 1 if total imported from the grid
#         is larger than 550,000 kWh.
#         """
#         lhs = self.total_import-550000
#         rhs = -self.MASSIVE_M * (1-self.above_550)
#         return lhs >= rhs+self.small_M 

  
#     @constraint()
#     def total_import_value_constraint(self):
#         """
#         This constraints tracks the total imports
#         """
#         imports = 0
#         for t in self.time:
#             imports += self.energy_imported[t]['Grid']
#         return self.total_import == imports
    
#     @constraint()
#     def total_export_value_constraint(self):
#         """
#         This constraints tracks the total exports from the panels which is funneled through 
#         GreenElec stream. 
#         """
#         exports = 0
#         for t in self.time:
#             exports += self.energy_exported[t]['GreenElec'] 
#         return self.total_export == exports

    
#     @constraint('billing_periods')
#     def monthly_import_constraint(self,period):
#         """
#         This constraints tracks the monthly imports from the grid
#         """
#         i=self.billing_periods.index(period)

#         imports=[self.energy_imported[t]['Grid'] 
#                  for t in self.time if ((t>=self.hours_list[i]) & (t<self.hours_list[i+1]))]
        
#         return self.monthly_import[period] == sum(imports)
    
#     @constraint('billing_periods')
#     def monthly_export_constraint(self,period):
#         """
#         This constraint tracks the monthly exports from the green electricity and the electricity.
#         """
#         i=self.billing_periods.index(period)

#         export=[self.energy_exported[t]['GreenElec'] 
#                 for t in self.time if ((t>=self.hours_list[i]) & (t<self.hours_list[i+1]))]
        
#         return self.monthly_export[period] == sum(export)
    
#     @constraint()
#     def net_metering_constraint(self):
#         """
#         Netmetering constraint
        
#         I've added a 0.1 due to the rounding error of the solver 
#         we something get a small amount of more exported than 
#         imported. The 0.1 adds a little bit that does not give 
#         us this error.
#         """
#         return self.total_export+0.1 <= self.total_import
    
#     ####### ROOF TOP CONSTRAINTS ####
    
    
#     ######## WORKER FUNCTIONS #######
    
#     def set_irr(self, i, irr):
#         """
#         Set the irradiance for the pv model
#         param: i - panel number
#         param: irr - an array with the irradience in kW per m2
#         """
#         self.TIME_SERIES['Irra{}'.format(i)] = {t: value for t, value in enumerate(irr)}
        
#     def set_lin_cap_cost(self, i, cost):
#         """
#         Set the linear capital cost for the pv system
#         param: i - panel number
#         param: cost - the linear cost of the panel in $/m2
#         """
#         self.LINEAR_CAPITAL_COSTS['PV{}'.format(i)] = cost
    
#     def set_load(self,Load):
#         """
#         Set the electricity load for each timestep 
#         param: Load - An array that has the hourly load of the building in kWh
#         """
#         new_elec = {t: value for t, value in enumerate(Load)}
#         self.LOADS['Elec'] = new_elec 
    
#     def make_pie_chart(self):
#         """
#         Cool analysis function, this takes the costBreak variable made after
#         evaluating the model using eval_model and makes a nice piechart. 
#         """
#         if (self.energyIm == 0):
#             print("No energy is imported from the grid")
#         else:
#             print("Total Electricity Imported from the grid {:,.0f} kWh".format(self.energyIm))
#             print("At a cost of $ {:,.4f} /kWh".format(self.costBreak["Energy Charge"]/self.energyIm))
#         print("Total Cost of the system: $ {:,.0f} per year".format(self.totCost))
        
#         cleaned_up_dict = self.costBreak
        
#         cleaned_up_dict={k:v for k,v in cleaned_up_dict.items() if round(v) != 0}
        
#         fig1, ax1 = plt.subplots()
#         fig1.set_facecolor('lightgrey')
        

#         ax1.pie(cleaned_up_dict.values(),\
#                 labels=cleaned_up_dict.keys(), autopct='%1.1f%%', \
#                 shadow=False, startangle=90);
#         ax1.axis('equal')  
#         plt.show();
        
#     def roof_percentage(self):
#         """
#         This finds the percentage of the roof coverage. 
#         """
        
#         total_roof_area      = self.solveResults['solution']['total_roof_area']
#         total_roof_area_used = self.solveResults['solution']['total_roof_area_used']
#         percentage       = (total_roof_area_used/total_roof_area)*100
#         print("Total roof area of the  building: {:,.0f} m²".format(total_roof_area))
#         print("Total roof area used {:,.0f} m²".format(total_roof_area_used))
#         print("Percentage: {:3.1f}%".format(percentage)) 
        
#     def generated_percentage(self):
#         """
#         This finds the total electricity needed by the building and how much the pv
#         panels contribute to this total amount. 
#         """
#         load_electricity = self.energyLoad
#         load_pro         = self.energyProd
#         percentage       = (load_pro/load_electricity)*100
#         print("Total Electricity Load from the building: {:,.0f} kWh".format(load_electricity))
#         print("Total Electricity Generated from PV: {:,.0f} kWh".format(load_pro))
#         print("Percentage: {:3.1f}%".format(percentage)) 
        
#     def eval_model(self,values,price,pvpanel):
#         '''
#         This is the evaluator function that run the EHub model based on a set 
#         of values that corrospond to the panel orientations. 
#         param: values   - set orientations for five panels
#         param: price    - the price of the pv panel
#         param: my_model - The EHub model
#         param: pvpanel  - The pv panel setup on the building
#         '''
#         print("Start Evaluation")
#         # Determine the number of panels used.
#         self.num_panels = len(self.TIME_SERIES.keys()) - 1 
        
#         azi  = values[0:self.num_panels]
#         tilt = values[self.num_panels:self.num_panels*2]
#         gcr  = values[self.num_panels*2:self.num_panels*3]
        
#         # subtract Elec load the rest of the timeseries are irradiances
#         area_per_panel = [pvpanel.pp['pvrow_depth']*pvpanel.pp['pvrow_width']/gcr[i] for i in range(self.num_panels)]
#         self.roof_area_per_unit_capacity = {"PV{}".format(i):area_per_panel[i] for i in range(self.num_panels)}
#         t1 = time()
#         # Replace them with the irradiance from pvfactors
#         for i in range(self.num_panels):
#             # here we replace model's old PV data with the new
#             print('Compiling and setting: Irra{} based on:\n \
#                                           an Azimuth of {:.0f}\n \
#                                           a tilt of {:.1f}\n \
#                                           and a gcr of {:.3f}'.format(i,azi[i],tilt[i],gcr[i]))
#             IRRADIANCE = pvpanel.irradiance(azi[i],tilt[i],gcr[i])
#             self.set_irr(i,IRRADIANCE/1000)
#             # Here we set the linear capital cost of the panels
#             self.set_lin_cap_cost(i, price)
#         t2 = time()
#         print('Generated the panel irradiance in {:3.0f}s'.format(t2-t1))
#         t1 = time()
#         print('Starting recompile')
#         self.recompile()
#         t2 = time()
#         print('Recompiled the model in {:3.0f}s'.format(t2-t1))
#         t1 = time()
#         print('Starting solver')
#         # Now we solve the model and get back our results
#         results = self.solve()
#         self.solveResults = results
#         t2 = time()
#         print('Solved the model in {:.0f}s'.format(t2-t1))
#         panel_relevance = [self.solveResults['solution']['capacity_tech']['PV{}'.format(i)] 
#                            for i in range(self.num_panels)]
#         print(panel_relevance)
        
#         # helper function
#         def sum_results(key,field):
#             length=len(self.solveResults['solution'][key])
#             sum_total=0
#             for i in range(length):
#                 sum_total+=self.solveResults['solution'][key][i][field]
#             return sum_total
        
        
#         # Total Cost of the System
#         Cost            = self.solveResults['solution']['total_cost']
        
#         energy_load = sum(self.solveResults['solution']['LOADS']['Elec'].values())
#         monthly_import_dict = self.solveResults['solution']['monthly_import_cost']
#         monthly_export_dict = self.solveResults['solution']['monthly_export_income']
#         energy_cost = sum([monthly_import_dict[period]- monthly_export_dict[period] for period in self.billing_periods] )
#         energy_impo = self.solveResults['solution']['total_import']
#         print(Cost)
#         totaldemandcost = sum(self.solveResults['solution']['demand_charge_c'].values())
#         print(totaldemandcost)
#         daily_cost      = self.solveResults['solution']['daily_charge_c']
#         print(daily_cost)
#         output_excel(results['solution'], self.resultsFile, 
#                      time_steps=len(self.time), sheets=['other', 'capacity_tech'])
        
#         cost_break = {'Levelized Costs':Cost-energy_cost-totaldemandcost-daily_cost,
#                       'Energy Charge':energy_cost,
#                       'Demand Charge':totaldemandcost,
#                       'Daily Charge' :daily_cost}

#         energy_produced = sum(results['solution']['energy_input']['Invertor']) 
        
        
       
#         self.Orientations = values
#         self.totCost      = Cost
#         self.totCapacity  = sum(panel_relevance)
#         self.panelCap     = panel_relevance
#         self.costBreak    = cost_break
#         self.energyProd   = energy_produced
#         self.energyLoad   = energy_load
#         self.energyIm     = energy_impo
#         self.roof_area_used  = self.solveResults['solution']['total_roof_area_used']
#         self.make_pie_chart()
#         self.roof_percentage()
        
# class ModelOn(pvRoofModel):
#     """
#     This is the subclass that deals with Ontario power.
#     We have discontinued the work on this bit. 
#     """
    
#     def __init__(self, *, excel=None, 
#                  request=None,
#                  max_carbon=0,
#                  MAX_ROOF_AREA=None,
#                  ROOF_AREA_CAP=None,
#                  resultsFile=None,
#                  EP_LOAD=None,
#                  NUM_PANELS=3):
#         super().__init__(excel=excel, 
#                          request=request,
#                          MAX_ROOF_AREA=MAX_ROOF_AREA,
#                          ROOF_AREA_CAP=ROOF_AREA_CAP)
#         self.num_panels = NUM_PANELS
#         self.set_load(EP_LOAD)
#         self.set_elec_ontario()
#         self.resultsFile = resultsFile
    
#     def set_elec_price(self,Cost):
#         """
#         Set the electricity price for each timestep
#         param: Cost - an Array that has the hourly electricity price 
#         """
#         new_cost = {t: value for t, value in enumerate(Cost)}
#         self.TIME_SERIES['Elec price'] = new_cost
        
#     def set_elec_ontario(self):
#         """
#         This calculates the 8760 hourly ontario electricity prices for the entire year
#         based on the the on, mid and off peak tarrifs. 
#         """
#         which_year = 2018

#         # In the hour ending in means, 1am, activity happens between 12pm and 1am or 24:00 and 01:00.
#         index = pd.date_range(start='1/1/{} 01:00'.format(which_year),
#                               end='1/1/{} 00:00'.format(which_year+1), freq='H').to_series()

#         # Electricity Cost
#         columns = ['cost']

#         # Make the dataframe
#         df_cost = pd.DataFrame(index=index, columns=columns)

#         # Fill with Nan's
#         df_cost = df_cost.fillna(0)

#         # Different peak prices for Ontario
#         self.offpeak_tarrif = 6.5/100
#         self.midpeak_tarrif = 9.4/100
#         self.onpeak_tarrif  = 13.4/100 

#         # summer 
#         df_summer = df_cost.loc['{}-5-1 01:00'.format(which_year):'{}-10-31'.format(which_year)]
#         df_cost.loc[(df_summer.between_time('20:00', '7:00').index)] = self.offpeak_tarrif
#         df_cost.loc[(df_summer.between_time('8:00', '11:00').index)] = self.midpeak_tarrif
#         df_cost.loc[(df_summer.between_time('12:00', '17:00').index)] = self.onpeak_tarrif
#         df_cost.loc[(df_summer.between_time('18:00', '19:00').index)] = self.midpeak_tarrif


#         # Winter
#         df_winter = df_cost[(df_cost.index > '{}-11-1 00:00'.format(which_year)) | (df_cost.index < '{}-5-1 01:00'.format(which_year))]
#         df_cost.loc[(df_winter.between_time('20:00', '7:00').index)] = self.offpeak_tarrif
#         df_cost.loc[(df_winter.between_time('8:00', '11:00').index)] = self.onpeak_tarrif
#         df_cost.loc[(df_winter.between_time('12:00', '17:00').index)] = self.midpeak_tarrif
#         df_cost.loc[(df_winter.between_time('18:00', '19:00').index)] = self.onpeak_tarrif

#         # Weekends
#         df_cost[(index.dt.dayofweek==5) | (index.dt.dayofweek==6)] = self.offpeak_tarrif

#         # Holydays https://www.statutoryholidays.com/ontario.php/
#         #  New Year’s Day, Family Day, Good Friday, Christmas Day, Boxing Day, Victoria Day, 
#         # Canada Day, Labour Day, Thanksgiving Day, and the Civic Holiday. When any holiday 
#         # falls on a weekend (Saturday or Sunday), the next weekday following (that is not 
#         # also a holiday) is to be treated as the holiday for RPP TOU pricing purpose
#         def set_holiday(df_cost,day,tarrif):
#             '''
#             param: day is a datetime object
#             param: tarrif is the taffir for the hours ending
#             '''
#             weeknum = day.weekday()
#             if ((weeknum==5) | (weeknum==6)):
#                 try_next_day = day+timedelta(days=1)
#                 df_cost = set_holiday(df_cost,try_next_day,tarrif)
#             else:
#                 df_cost.loc["{}-{}-{}".format(day.year,day.month,day.day)] = tarrif
#             return df_cost


#         holidays_list = ['1-1',    # new years
#                          '2-18',  # family day
#                          '4-19',   # good friday
#                          '5-20',   # victoria day
#                          '6-1',    # Canada Day
#                          '8-5',    # Civic Holiday
#                          '9-2',    # Labour day
#                          '10-14',  # Thanks Giving
#                          '11-11',  # Rememberance Day
#                          '12-25',  # Christmas
#                          '12-26'  ] # Boxing day

#         holidays = ['{}-{}'.format(which_year,holidays_list[i]) for i in range(len(holidays_list))]

#         for holiday in holidays:
#             day = datetime.strptime(holiday, '%Y-%m-%d')
#             df_cost = set_holiday(df_cost,day,self.offpeak_tarrif)
#         self.hourly_on_electricity_prices = df_cost['cost']
#         self.set_elec_price(df_cost['cost'])
    
#     def eval_model(self,values,price,pvpanel):
#         '''
#         This is the evaluator function that run the EHub model based on a set 
#         of values that corrospond to the panel orientations. 
#         param: values   - set orientations for five panels
#         param: price    - the price of the pv panel
#         param: my_model - The EHub model
#         param: pvpanel  - The pv panel setup on the building
#         '''
#         print("Start Evaluation")
#         azi  = values[0:5]
#         tilt = values[5:10]
#         gcr  = values[10:15]
       
#         # Determine the number of panels used.
#         # subtract Elec load and price the rest of the timeseries are irradiances
#         area_per_panel = [pvpanel.pp['pvrow_depth']*pvpanel.pp['pvrow_width']/gcr[i] for i in range(self.num_panels)]
#         self.roof_area_per_unit_capacity = {"PV{}".format(i):area_per_panel[i] for i in range(self.num_panels)}
#         t1 = time()
#         # Replace them with the irradiance from pvfactors
#         for i in range(self.num_panels):
#             # here we replace model's old PV data with the new
#             print('Compiling and setting: Irra{}'.format(i))
#             IRRADIANCE = pvpanel.irradiance(azi[i],tilt[i],gcr[i])
#             self.set_irr(i,IRRADIANCE/1000)
#             # Here we set the linear capital cost of the panels
#             self.set_lin_cap_cost(i, price)
#         t2 = time()
#         print('Generated the panel irradiance in {:3.0f}s'.format(t2-t1))
#         t1 = time()
#         print('Starting recompile')
#         self.recompile()
#         t2 = time()
#         print('Recompiled the model in {:3.0f}s'.format(t2-t1))
#         t1 = time()
#         print('Starting solver')
#         results=self.solve()
#         t2 = time()
#         print('Solved the model in {:3.0f}s'.format(t2-t1))
#         panel_relevance = [results['solution']['capacity_tech']['PV{}'.format(i)] for i in range(5)]
#         print(panel_relevance)
#         Cost= results['solution']['total_cost']
#         print(Cost)
#         output_excel(results['solution'], self.resultsFile,
#                      time_steps=len(self.time), 
#                      sheets=['other', 'capacity_storage', 'capacity_tech'])
#         total_on  = 0
#         total_mid = 0
#         total_off = 0
#         for i in range(len(results['solution']['energy_imported'])):
#             elec_price = results['solution']['TIME_SERIES']['Elec price'].iloc[i]
#             elec_use   = results['solution']['energy_imported'].iloc[i]
#             if (elec_price==self.onpeak_tarrif):
#                 total_on  += elec_use*elec_price
#             elif (elec_price==self.midpeak_tarrif):
#                 total_mid += elec_use*elec_price
#             else:
#                 total_off += elec_use*elec_price

                
#         cost_break = {'On Peak':total_on[0],
#                       'Mid Peak':total_mid[0],
#                       'Off Peak':total_off[0]}
#         try:
#             # Try this, with netmetering we do export the GreenElec
#             energy_produced = sum(results['solution']['energy_input']['GreenEng']+\
#                               results['solution']['energy_exported']['GreenElec']) 
#         except:
#             # If we get an error we don't have net metering
#             # there fore all the energy input will be the generated electricity. 
#             energy_produced = sum(results['solution']['energy_input']['GreenEng'])            
        
#         energy_load = sum(results['solution']['LOADS']['Elec']) 
#         energy_impo = sum(results['solution']['energy_imported']['Grid'])
        
#         self.Orientations = values
#         self.totCost      = Cost
#         self.totCapacity  = sum(panel_relevance)
#         self.panelCap     = panel_relevance
#         self.costBreak    = cost_break
#         self.energyProd   = energy_produced
#         self.energyLoad   = energy_load
#         self.energyIm     = energy_impo
#         self.solveResults = results