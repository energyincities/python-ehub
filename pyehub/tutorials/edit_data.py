"""
This is a file for a tutorial on how to extend the model without modifying it.

To run this file, from the root directory of this project, do

    python3.7 -m pyehub.tutorials.edit_data
"""
import os

import pandas as pd

from pyehub.energy_hub.ehub_model import EHubModel
from pyehub.outputter import print_section


def get_file_path(local_path) -> str:
    # This is a cross-platform way of getting the path to the Excel file
    current_directory = os.path.dirname(os.path.realpath(__file__))

    return os.path.join(current_directory, local_path)


def main():

    excel_file = 'test_file_all_constraints_work.xlsx'

    # Here's where we instantiate our model. Nothing is solved at this point.
    my_model = EHubModel(excel=excel_file)

    # The model is currently using specified excel file's contents, but we want to
    # use another file's data for the LOADS(say).
    # Make a dataframe from the second excel file.
    dataframe = pd.read_excel('8760_loads.xlsx', dtype=float)

    start_days = [0, 50, 100, 150, 200, 250, 300, 350]
    start_times = [start_day * 24 for start_day in start_days]

    results = []
    for start_time in start_times:
        # We enumerate the data so that's it in the range 0..23
        elec_data = dataframe['Elec'][start_time:start_time+24]
        new_elec = {t: value for t, value in enumerate(elec_data)}
        my_model.LOADS['Elec'] = new_elec # here we replace model's old Elec LOAD data
        # by new one we got from the new excel.

        heat_data = dataframe['Heat'][start_time:start_time+24]
        new_heat = {t: value for t, value in enumerate(heat_data)}
        my_model.LOADS['Heat'] = new_heat # here we replace model's old Heat LOAD data
        # by new one we got from the new excel.

        # Recompile since it would keep the constraints from the last run.
        my_model.recompile()

        results.append(my_model.solve())

    for i, result in enumerate(results):
        solution = result['solution']

        print_section(f'Solution {i} starting at time t = {start_times[i]}', {
            'total_cost': solution['total_cost'],
            'LOADS': solution['LOADS'],
        })


# If we are being run as a script
if __name__ == '__main__':
    main()
