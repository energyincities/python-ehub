# Change Request Format

This is a high-level step-by-step on how to add another row to the Excel file.

1. Add the row to the Excel file.
2. Change the `excel_to_request_format.py` file to convert the new Excel file
3. Update the request format schema
4. Check that the model still runs correctly
5. Do whatever you wanted to do with this new row.
