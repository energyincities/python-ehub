import pytest
import os
import numpy as np
import pdb

from energy_hub import EHubModel
from multi_run_period import run_split_period

def get_file(excel_file):
    current_directory = os.path.dirname(os.path.realpath(__file__))
    excel = os.path.join(current_directory, excel_file)
    return excel

excel_file = 'two_day_test_file.xlsx'
input_file = get_file(excel_file)

def actual_simulation(excel):
    hub = EHubModel(excel=excel)
    results = hub.solve()
    return results['solution']['total_cost']

ACTUAL_RESULTS = 1863.83

def test_no_split():
    """
    Sanity check that doing no splitting through multi_run_period returns the same values as the results of just doing a straight run with EHub's solve.
    """
    split_results= run_split_period(excel=input_file, num_periods=1, len_periods=48, num_periods_in_sample_period=1, solver='glpk')
    assert round(split_results[2],2) == ACTUAL_RESULTS

def test_multiple_periods():
    split_results = run_split_period(excel=input_file, num_periods=2, len_periods=24, num_periods_in_sample_period=1, solver='glpk')
    assert split_results[2] == 1863.8259400000002

def test_period_sampling():
    split_results = run_split_period(excel=input_file, num_periods=1, len_periods=24, num_periods_in_sample_period=2, solver='glpk')
    assert split_results[2] == 1863.82594