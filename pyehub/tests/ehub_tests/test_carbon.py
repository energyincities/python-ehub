import pytest

import os
from energy_hub import EHubModel
from multiple_hubs import multiple_hubs
import network_to_request_format
from energy_hub.input_data import InputData

"""
Tests for the carbon capped EHubModel 

To run all the tests:
    $ pytest 

Running pytest will run all the test that are found in python-ehub, ie. will run the tests for network as well.

To run just the EHubModel tests:
    $ pytest -m "not network" 
"""


def get_file(excel_file):
    current_directory = os.path.dirname(os.path.realpath(__file__))
    excel = os.path.join(current_directory, excel_file)
    return excel


def test_normal_model():
    excel = get_file("test_file.xlsx")
    model = EHubModel(excel=excel)
    results = model.solve()
    assert (results['solution']['total_cost'] == 1846.19 and results['solution']['total_carbon'] == 33.7551)

def test_carbon_constraint_flag():
    excel = get_file("test_file.xlsx")
    model = EHubModel(excel=excel, max_carbon=15)
    results = model.solve()
    assert results['solution']['total_cost'] == 2143.96 and results['solution']['total_carbon'] == 15

def test_min_carbon():
    excel = get_file("test_file.xlsx")
    model = EHubModel(excel=excel)
    model.compile()
    model.objective = 'total_carbon'
    results = model.solve()
    assert results['solution']['total_cost'] == 6112240.0 and results['solution']['total_carbon'] == -264.37

def test_network():
    hubs = get_file('hub__')
    network = get_file('network2.xlsx')
    n=3
    results = multiple_hubs(input_files=hubs, n=n, network_excel=network)
    absolute_carbon = 0
    absolute_cost = 0
    for i in range(n):
        absolute_carbon += results[i]['total_carbon']
        absolute_cost += results[i]['total_cost']
    assert absolute_carbon == 41.7077 and absolute_cost == 26259.97

def test_network_carbon():
    hubs = get_file('hub__')
    network = get_file('network2.xlsx')
    n=3
    results = multiple_hubs(input_files=hubs, n=n, network_excel=network, max_carbon=1)
    absolute_carbon = 0
    absolute_cost = 0
    for i in range(n):
        absolute_carbon += results[i]['total_carbon']
        absolute_cost += results[i]['total_cost']
    assert absolute_carbon == 3 and absolute_cost == 33171.99