"""
Adding path to pyehub root directory to ensure tests can find pyehub modules.
"""
import os
import inspect
import sys
CURRENT_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
TEST_DIR= os.path.dirname(CURRENT_DIR)
ROOT_DIR = os.path.dirname(TEST_DIR)
sys.path.append(ROOT_DIR) 