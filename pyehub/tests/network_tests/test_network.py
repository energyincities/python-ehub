import pytest
from multiple_hubs import multiple_hubs
import network_to_request_format
from energy_hub.input_data import InputData

"""
Tests for multiple_hubs

To run all the tests:
    $ pytest 

Running pytest will run all the test that are found in python-ehub, ie. will run the tests for EHubModel as well. 

To run just the network tests:
    $ pytest -m network
"""


def calc_results(input_file, n, network):
    """Returns the solution of the model. n = number of hubs"""
    sol = multiple_hubs(input_files=input_file, n=n, network_excel=network)
    return sol


def network_info_dic(excel_file):
    """Returns dic with all the information of the network excel file """
    network = network_to_request_format.convert(excel_file)
    data = InputData(network)
    return data


@pytest.mark.network
def test_unidirectional():
    """Tests that the model is unidirectional by default. """
#--------------------------------------------------------------------------------------------------------
    # Simple network system
    input_files = 'pyehub/tests/network_tests/hub_'
    # number of hubs
    n = 2
    # excel file defines 1 link 1->0
    network_excel_file = 'pyehub/tests/network_tests/network1.xlsx'
    solution=calc_results(input_files, n, network_excel_file)

    sol_hub1 = solution[0] # solution of hub1
    sol_hub2 = solution[1] # solution of hub2

    energy_exported1 = sol_hub1["energy_exported"]
    energy_exported2 = sol_hub2["energy_exported"]
    energy_imported1 = sol_hub1["energy_imported"]
    energy_imported2 = sol_hub2["energy_imported"]
    time = sol_hub1['time']

    # Connection 1->0 should not be used(not profitable). Checking that the connection not used in the other direction.
    for t in time:
        assert energy_exported1[t]['Net_export'] == 0
        assert energy_imported1[t]['Net_import'] == 0
        assert energy_exported2[t]['Net_export'] == 0
        assert energy_imported2[t]['Net_import'] == 0

    assert sol_hub1["network_cost"] == 0
    assert sol_hub2["network_cost"] == 0

    assert sol_hub1["is_link_installed"][0] == 0

# ----------------------------------------------------------------------------------------------------------
    # Bigger network system
    input_files = 'pyehub/tests/network_tests/hub'
    n = 3
    network_excel_file = 'pyehub/tests/network_tests/network2.xlsx'
    sol = calc_results(input_files, n, network_excel_file)

    hub1 = sol[0] # solution of hub1
    hub2 = sol[1] # solution of hub2
    hub3 = sol[2] # solution of hub3

    e_export1 = hub1["energy_exported"]
    e_export2 = hub2["energy_exported"]
    e_export3 = hub3["energy_exported"]
    e_import1 = hub1["energy_imported"]
    e_import2 = hub2["energy_imported"]
    e_import3 = hub3["energy_imported"]
    time = hub1["time"]

    # Checking that the connections are installed
    for i in range(2):
        assert hub1["is_link_installed"][i] == 1

    # Checking that the connections only work in the direction that set in the network excel file
    for t in time:
        assert e_export1[t]["Net_export"] != 0
        assert e_import1[t]["Net_import"] == 0

        assert e_export2[t]["Net_export"] == 0
        assert e_import2[t]["Net_import"] != 0

        assert e_export3[t]["Net_export"] == 0
        assert e_import3[t]["Net_import"] != 0

    total_network_cost = hub1["network_cost"] + hub2["network_cost"] + hub3["network_cost"]
    assert total_network_cost == 112.6954


@pytest.mark.network
def test_bidirectional():
    """Tests that the model can be bidirectional if the connections defined in two directions"""
    input_files = 'pyehub/tests/network_tests/hub__'
    n = 3
    network_excel_file = 'pyehub/tests/network_tests/network3.xlsx'

    sol = calc_results(input_files, n, network_excel_file)

    hub1 = sol[0] # solution of hub1
    hub2 = sol[1] # solution of hub2
    hub3 = sol[2] # solution of hub3

    e_export1 = hub1["energy_exported"]
    e_export2 = hub2["energy_exported"]
    e_export3 = hub3["energy_exported"]
    e_import1 = hub1["energy_imported"]
    e_import2 = hub2["energy_imported"]
    e_import3 = hub3["energy_imported"]

    # Check all connections are installed
    for i in range(4):
        assert hub1["is_link_installed"][i] == 1

    # Check that hubs can import and export to network
    for t in range(13, 18):
        assert e_export1[t]["Net_export"] !=0

    for t in range(8):
        assert e_export2[t]["Net_export"] != 0

    assert e_export2[16]["Net_export"] != 0

    for t in range(8,12):
        assert e_export3[t]["Net_export"] != 0

    for t in range(0,13):
        assert e_import1[t]["Net_import"] !=0

    assert e_import1[16]["Net_import"] !=0

    for t in range(8,18):
        assert e_import2[t]["Net_import"] != 0

    for t in range(0,9):
        assert e_import3[t]["Net_import"] != 0

    for t in range(13,18):
        assert e_import3[t]["Net_import"] != 0

    # Check capacities of links
    assert hub1["capacity0"] == 14.0855
    assert hub1["capacity1"] == 4.72502
    assert hub1["capacity2"] == 8.71809
    assert hub1["capacity3"] == 11.7445


@pytest.mark.network
def test_network_investment_cost():
    """Test that the investment cost calculated correctly"""
    input_files = ['pyehub/tests/network_tests/hub__', 'pyehub/tests/network_tests/hub']
    n=3
    network_excel_file = 'pyehub/tests/network_tests/network3.xlsx'

    for files in input_files:
        sol = calc_results(files, n, network_excel_file)

        hub1 = sol[0]  # solution of hub1
        hub2 = sol[1]  # solution of hub2
        hub3 = sol[2]  # solution of hub3

        # capacities of the links:
        capacity1 = hub1["capacity0"]
        capacity2 = hub1["capacity1"]
        capacity3 = hub1["capacity2"]
        capacity4 = hub1["capacity3"]

        # lengths of the links:
        length1 = hub1["LINK_LENGTH"][0]
        length2 = hub1["LINK_LENGTH"][1]
        length3 = hub1["LINK_LENGTH"][2]
        length4 = hub1["LINK_LENGTH"][3]

        # Fixed prices
        FIXED_NETWORK_INVESTMENT_COST = hub1['FIXED_NETWORK_INVESTMENT_COST']
        LINK_PROPORTIONAL_COST = hub1['LINK_PROPORTIONAL_COST']

        # network cost of hub1
        network_cost1 = ((FIXED_NETWORK_INVESTMENT_COST * length1 * hub1["is_link_installed"][0]
                         + LINK_PROPORTIONAL_COST * length1 * capacity1)/2) + \
                        ((FIXED_NETWORK_INVESTMENT_COST * length2 * hub1["is_link_installed"][1]
                         + LINK_PROPORTIONAL_COST * length2 * capacity2)/2)
        assert round(network_cost1,3) == round(hub1["network_cost"],3)

        # network cost of hub2
        network_cost2 = ((FIXED_NETWORK_INVESTMENT_COST * length1 * hub1["is_link_installed"][0]
                         + LINK_PROPORTIONAL_COST * length1 * capacity1)/2) + \
                        ((FIXED_NETWORK_INVESTMENT_COST * length2 * hub1["is_link_installed"][1]
                         + LINK_PROPORTIONAL_COST * length2 * capacity2)/2) + \
                        ((FIXED_NETWORK_INVESTMENT_COST * length3 * hub1["is_link_installed"][2]
                          + LINK_PROPORTIONAL_COST * length3 * capacity3) / 2) + \
                        ((FIXED_NETWORK_INVESTMENT_COST * length4 * hub1["is_link_installed"][3]
                          + LINK_PROPORTIONAL_COST * length4 * capacity4) / 2)
        assert round(network_cost2,3) == round(hub2["network_cost"],3)

        # network cost of hub3
        network_cost3 = ((FIXED_NETWORK_INVESTMENT_COST * length3 * hub1["is_link_installed"][2]
                         + LINK_PROPORTIONAL_COST * length3 * capacity3)/2) + \
                        ((FIXED_NETWORK_INVESTMENT_COST * length4 * hub1["is_link_installed"][3]
                         + LINK_PROPORTIONAL_COST * length4 * capacity4)/2)
        assert round(network_cost3,3) == round(hub3["network_cost"],3)


@pytest.mark.network
def test_thermal_losses():
    """Tests that thermal losses in the links work and calculate the energy correctly"""
# -----------------------------------------------------------------------------------------------------------
    # Simple system (2 hubs, 1 link)
    input_files = 'pyehub/tests/network_tests/hub_'
    n = 2
    network_excel_file = 'pyehub/tests/network_tests/network4.xlsx'
    network_data = network_info_dic(network_excel_file)

    sol = calc_results(input_files, n, network_excel_file)

    hub1 = sol[0]  # solution of hub1
    hub2 = sol[1]  # solution of hub2

    link_thermal_losses = network_data.link_thermal_loss[0]

    e_exported1 = hub1["energy_exported"]
    e_imported2 = hub2["energy_imported"]
    time = hub1["time"]

    for t in time:
        assert round(e_exported1[t]["Net_export"] * link_thermal_losses, 3) == e_imported2[t]["Net_import"]

# ------------------------------------------------------------------------------------------------------------
    # Bigger system (3 hubs, 2 links)

    input_files = 'pyehub/tests/network_tests/hub__'
    n = 3
    network_excel_file = 'pyehub/tests/network_tests/network5.xlsx'
    network_data = network_info_dic(network_excel_file)

    sol = calc_results(input_files, n, network_excel_file)

    hub1 = sol[0] # solution of hub1
    hub2 = sol[1] # solution of hub2
    hub3 = sol[2] # solution of hub3

    thermal_losses1 = network_data.link_thermal_loss[0] # thermal losses of link1
    thermal_losses2 = network_data.link_thermal_loss[1] # thermal losses of link2

    e_exported1 = hub1["energy_exported"]
    e_imported2 = hub2["energy_imported"]
    e_exported2 = hub2["energy_exported"]
    e_imported3 = hub3["energy_imported"]
    time = hub1["time"]

    for t in time:
        assert round((e_exported1[t]["Net_export"] * thermal_losses1 - e_imported2[t]["Net_import"]) * thermal_losses2
                    + e_exported2[t]["Net_export"] * thermal_losses2 - e_imported3[t]["Net_import"] , 3) == 0


@pytest.mark.network
def test_network_energy_balance():
    """Tests energy balance, energy neither created nor destroyed when using a network connection"""
    input_files = 'pyehub/tests/network_tests/hub-'
    n = 4
    network_excel_file = 'pyehub/tests/network_tests/network6.xlsx'

    sol = calc_results(input_files, n, network_excel_file)

    hub1 = sol[0]  # solution of hub1
    hub2 = sol[1]  # solution of hub2
    hub3 = sol[2]  # solution of hub3
    hub4 = sol[3]  # solution of hub4

    for i in range(5):
        if i != 2:
            assert hub1["is_link_installed"][i] == 1

    e_exported1 = hub1["energy_exported"]
    e_exported2 = hub2["energy_exported"]
    e_exported3 = hub3["energy_exported"]
    e_exported4 = hub4["energy_exported"]
    e_imported1 = hub1["energy_imported"]
    e_imported2 = hub2["energy_imported"]
    e_imported3 = hub3["energy_imported"]
    e_imported4 = hub4["energy_imported"]
    time = hub1["time"]

    # Checking energy balance between the hubs
    for t in time:
        assert round(e_exported1[t]["Net_export"] + e_exported2[t]["Net_export"] + e_exported3[t]["Net_export"] + \
               e_exported4[t]["Net_export"] - e_imported1[t]["Net_import"] - e_imported2[t]["Net_import"] - \
               e_imported3[t]["Net_import"] - e_imported4[t]["Net_import"], 3)== 0


@pytest.mark.network
def test_connections():
    """Tests that if there is connections 0->1 and 1->2, there is no need to define 0->2,
    the model will be able export from 0 to 2 through 1"""

    input_files = 'pyehub/tests/network_tests/hub~'
    n = 4
    network_excel_file = 'pyehub/tests/network_tests/network7.xlsx'

    sol = calc_results(input_files, n, network_excel_file)

    hub1 = sol[0]  # solution of hub1
    hub2 = sol[1]  # solution of hub2
    hub3 = sol[2]  # solution of hub3
    hub4 = sol[3]  # solution of hub4

    for i in range(3):
        assert hub1["is_link_installed"][i] == 1

    e_exported1 = hub1["energy_exported"]
    e_exported2 = hub2["energy_exported"]
    e_exported3 = hub3["energy_exported"]
    e_exported4 = hub4["energy_exported"]
    e_imported1 = hub1["energy_imported"]
    e_imported2 = hub2["energy_imported"]
    e_imported3 = hub3["energy_imported"]
    e_imported4 = hub4["energy_imported"]
    time = hub1["time"]

    # Checks that hub0 can export to all hubs through a chain of connections
    # 0->1->2->3
    for t in time:
        assert e_imported1[t]["Net_import"] == 0
        assert e_exported1[t]["Net_export"] != 0
        assert e_imported2[t]["Net_import"] != 0
        assert e_exported2[t]["Net_export"] == 0
        assert e_imported3[t]["Net_import"] != 0
        assert e_exported3[t]["Net_export"] == 0
        assert e_imported4[t]["Net_import"] != 0
        assert e_exported4[t]["Net_export"] == 0

        assert round(e_imported2[t]["Net_import"] + e_imported3[t]["Net_import"] +e_imported4[t]["Net_import"], 3) \
               ==  round(e_exported1[t]["Net_export"], 3)


# ------------------------------------------------------------------------------------------------------------
    # Bigger system with linear power flows (3 hubs, 2 links)