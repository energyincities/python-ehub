PyHub
=====

A program to solve an energy hub model in Python.

Requirements
------------

- Python 3.7.3
- pip for Python 3.7.3
- `GLPK` or another solver supported by PyLP

Running a Model
---------------

To run a energy hub model, use the `General_input.xlsx` file to enter your data.
The data **must** be in the same format.

Check the `config.yaml` file to see if the input file and output file is where
you want to load and output the results of the model respectively.

While inside the `config.yaml` file, be sure to set the settings for using a
specific solver that is installed on your system.
The default one is `glpk` with some options, but you can set it to any other
solver that is supported by PyLP.

Once you have configured the `config.yaml` file, run:
```
python run.py
```
to solve the model.
The results should be in the file you specified in `config.yaml`.

Development
-----------

### Installation

To install PyHub, either pip install PyEHub
or download the repo and its requirements directly.

Pip installing PyEHub:

```
pip install pyehub
```

Download the repo:
```
git clone https://gitlab.com/energyincities/python-ehub
```

Install the libraries needed for PyEHub to run:
```
pip install -r requirements.txt
```

Also install GLPK or another Pulp supporting solver.
Can be found [here](https://www.gnu.org/software/glpk/).
Windows installation instructions [here](http://www.osemosys.org/uploads/1/8/5/0/18504136/glpk_installation_guide_for_windows10_-_201702.pdf)


Edit the `config.yaml` file to use a selected solver.

Run the `run.py` script to see if everything works.
```
python run.py
```

### Docs

Can be found [here](docs/explanation.md).

Contributing
------------

### Features/Bug fixes

If you are fixing a bug or making a new feature, first get the lastest master branch.
```
git checkout master
git pull
```

Then create your own branch for you to work on:
```
git branch <your-branch-name>
git checkout <your-branch-name>
```

Once you are done, please submit a pull request.


